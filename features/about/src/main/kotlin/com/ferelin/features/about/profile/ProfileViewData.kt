package com.ferelin.features.about.profile

internal data class ProfileViewData(
  val companyName: String = "",
  val logoUrl: String = "",
  val country: String = "",
  val phone: String = "",
  val webUrl: String = "",
  val industry: String = "",
  val capitalization: String = ""
)