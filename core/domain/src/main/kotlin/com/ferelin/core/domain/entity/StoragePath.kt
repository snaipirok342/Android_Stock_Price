package com.ferelin.core.domain.entity

data class StoragePath(
  val path: String,
  val authority: String
)