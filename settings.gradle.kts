rootProject.name = "Stock Price"
include(
  ":app",
  ":core",
  ":core:ui",
  ":core:data",
  ":core:domain",
  ":features:about",
  ":features:stocks",
  ":features:splash",
  ":features:authentication",
  ":features:settings",
  ":features:search",
)