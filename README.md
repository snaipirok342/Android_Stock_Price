# Android Stock Price

## About
An application whose main purpose is to display stock prices. Using the chart, you can analyze the history of price changes. It is possible to add a stock to your favorites list. You can log into the app through your phone number. In this case, all favourite stocks and search requests will be saved in the cloud. 

App API: [Stocks](https://finnhub.io/) | [Crypto](https://nomics.com/docs/)

Google Play[XML VERSION]: [Stock Price](https://play.google.com/store/apps/details?id=com.ferelin.stockprice)


## Stack
- Jetpack Compose only. [XML version](https://github.com/NikitaFerelin/Android_Stock_Price/tree/18670da6124d0328b0d323b675d0bafdb4df45f6)
- Clean Architecture try
- MVVM / MVI
- Kotlin Flow
- Room
- Dagger2
- Glide
- Data Store Preferences
- Moshi
- Retrofit + okHttp
- Firebase Realtime Db
- Firebase Authorization


## Compose UI
<p float="middle">
<img src="https://github.com/NikitaFerelin/Android_Stock_Price/blob/master/dev-preview/Preview_1.gif" height="400" width = "200"/>
<img src="https://github.com/NikitaFerelin/Android_Stock_Price/blob/master/dev-preview/Preview_2.gif" height="400" width = "200"/>
</p>

## XML Version
<p float="middle">
<img src="https://github.com/NikitaFerelin/Android_Stock_Price/blob/master/dev-preview/XML_preview_1.gif" height="400" width = "200"/>
  <img src="https://github.com/NikitaFerelin/Android_Stock_Price/blob/master/dev-preview/XML_preview_2.gif" height="400" width = "200"/>
  <img src="https://github.com/NikitaFerelin/Android_Stock_Price/blob/master/dev-preview/XML_preview_3.gif" height="400" width = "200"/>
</p>


## Phone numbers for test login
- +16505555555 . Code [123456]
- +16504444444 . Code [123456]
- +16503333333 . Code [123456]
